import pylab as pl
import numpy as np
import numpy.linalg as ln
import re
import sympy as sym


class SGroup:
    def __init__(self, _sgroup, _orbs='', spin=1):
        self.sgroup = _sgroup
        self.GtoOrb_done = {}

        print("sgroup: ", self.sgroup)
        if self.sgroup == 139:
            # Matrix changing basis from cartesian to the space
            # of translational lattice vectors
            self.R1 = pl.transpose(pl.matrix(((1 / 2., 1 / 2., -1 / 2.),
                                              (-1 / 2., 1 / 2., 1 / 2.),
                                              (1 / 2., -1 / 2., 1 / 2.))))
            # Inverse of the matrix
            self.R2 = ln.inv(self.R1)
            # ?
            self.B = pl.transpose(self.R2)

            # Cartesian symmetries of the crystal structure
            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'Sp+I', 'Sm+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'Sp+R', 'Sm+R',
                       'C1+I', 'C2+I', 'C3+I', 'C1+R', 'C2+R', 'C3+R']

            self.gO = {"": "I+I+I", "sx": "R+I+I", "sy": "I+R+I",
                       "sx*C4": "Sp+I", "sy*C4": "Sm+I", "sz": "I+I+R",
                       "sx*sz": "R+I+R", "sy*sz": "I+R+R",
                       "sz*sx*C4": "Sp+R", "sz*sy*C4": "Sm+R",
                       "C4": "C1+I", "C4*C4": "C2+I", "C4*C4*C4": "C3+I",
                       "C4*sz": "C1+R", "C4*C4*sz": "C2+R",
                       "C4*C4*C4*sz": "C3+R"}
            self.gO = {"I+I+I": "", "R+I+I": "sx", "I+R+I": "sy",
                       "Sp+I": "sx*C4", "Sm+I": "sy*C4", "I+I+R": "sz",
                       "R+I+R": "sx*sz", "I+R+R": "sy*sz",
                       "Sp+R": "sz*sx*C4", "Sm+R": "sz*sy*C4",
                       "C1+I": "C4", "C2+I": "C4*C4", "C3+I": "C4*C4*C4",
                       "C1+R": "C4*sz", "C2+R": "C4*C4*sz",
                       "C3+R": "C4*C4*C4*sz"}

            self.spaceGen = {"C4": np.matrix([[0, -1, 0],
                                              [1, 0, 0],
                                              [0, 0, 1]]),
                             "sx": np.diag([-1, 1, 1]),
                             "sy": np.diag([1, -1, 1]),
                             "sz": np.diag([1, 1, -1])}

            self.spinGen = {"C4":
                            np.matrix([[(1 - 1j)/np.sqrt(2), 0],
                                       [0, (1 + 1j)/np.sqrt(2)]]),
                            "sx": np.matrix([[0, 1j], [1j, 0]]),
                            "sy": np.matrix([[0, -1], [1, 0]]),
                            "sz": np.matrix([[1j, 0], [0, -1j]])}

            self.irrep = None

            # Character table of the space group
            self.irreps_table = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                                 [1, -1, -1, -1, -1, 1, 1, 1, 1, 1],
                                 [1, 1, 1, -1, -1, -1, -1, 1, 1, 1],
                                 [1, -1, -1, 1, 1, -1, -1, 1, 1, 1],
                                 [2, 0, 0, 0, 0, 0, 0, 2, -2, -2],
                                 [1, 1, -1, 1, -1, -1, 1, -1, 1, -1],
                                 [1, -1, 1, -1, 1, -1, 1, -1, 1, -1],
                                 [1, 1, -1, -1, 1, 1, -1, -1, 1, -1],
                                 [1, -1, 1, 1, -1, 1, -1, -1, 1, -1],
                                 [2, 0, 0, 0, 0, 0, 0, -2, -2, 2]]

            # Associated irreps
            self.irreps = [r"$A^+_{1g}$", r"$A^+_{2g}$", r"$B^+_{1g}$",
                           r"$B^+_{2g}$", r"$E^+_g$", r"$A^+_{1u}$",
                           r"$A^+_{2u}$", r"$B^+_{1u}$", r"$B^+_{2u}$",
                           r"$E^+_u$"]

            # ?
            self.irreps_op = [["I+I+I"], ["I+R+R", "R+I+R"],
                              ["R+I+I", "I+R+I"], ["Sp+R", "Sm+R"],
                              ["Sp+I", "Sm+I"], ["C1+R", "C3+R"],
                              ["C1+I", "C3+I"], ["C2+R"], ["C2+I"], ["I+I+R"]]

        elif self.sgroup == 71:
            self.R1 = pl.transpose(pl.matrix(((1 / 2., 1 / 2., -1 / 2.),
                                              (-1 / 2., 1 / 2., 1 / 2.),
                                              (1 / 2., -1 / 2., 1 / 2.))))
            self.R2 = ln.inv(self.R1)
            self.B = pl.transpose(self.R2)

            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'C2+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'C2+R']

            self.gO = {"": "I+I+I", "sx": "R+I+I", "sy": "I+R+I",
                       "sz": "I+I+R",
                       "sx*sz": "R+I+R", "sy*sz": "I+R+R",
                       "sx*sy": "C2+I", "sx*sy*sz": "C2+R"}
            self.gO = {"I+I+I": "", "R+I+I": "sx", "I+R+I": "sy",
                       "I+I+R": "sz",
                       "R+I+R": "sx*sz", "I+R+R": "sy*sz",
                       "C2+I": "sx*sy", "C2+R": "sx*sy*sz"}

            self.spaceGen = {"sx": np.diag([-1, 1, 1]),
                             "sy": np.diag([1, -1, 1]),
                             "sz": np.diag([1, 1, -1])}

            self.spinGen = {"sx": np.matrix([[0, 1j], [1j, 0]]),
                            "sy": np.matrix([[0, -1], [1, 0]]),
                            "sz": np.matrix([[1j, 0], [0, -1j]])}

            self.irrep = None

            # Character table of the space group
            self.irreps_table = [[1, 1, 1, 1, 1, 1, 1, 1],
                                 [1, 1, -1, -1, 1, 1, -1, -1],
                                 [1, -1, -1, 1, 1, -1, 1, -1],
                                 [1, -1, 1, -1, 1, -1, -1, 1],
                                 [1, 1, 1, 1, -1, -1, -1, -1],
                                 [1, 1, -1, -1, -1, -1, 1, 1],
                                 [1, -1, -1, 1, -1, 1, -1, 1],
                                 [1, -1, 1, -1, -1, 1, 1, -1]]

            # Associated irreps
            self.irreps = [r"$A^+_{g}$", r"$B^+_{1g}$",
                           r"$B^+_{2g}$", r"$B^+_[3g}$",
                           r"$A^+_{u}$", r"$B^+_{1u}$",
                           r"$B^+_{2u}$", r"$B^+_[3g]$"]

            # ?
            self.irreps_op = [["I+I+I"], ["C2+I"], ["I+R+R"], ["R+I+R"],
                              ["I+I+R"], ["C2+R"], ["I+R+I"], ["R+I+I"]]

        elif self.sgroup == 97:
            self.R1 = pl.transpose(pl.matrix(((1 / 2., 1 / 2., -1 / 2.),
                                              (-1 / 2., 1 / 2., 1 / 2.),
                                              (1 / 2., -1 / 2., 1 / 2.))))
            self.R2 = ln.inv(self.R1)
            self.B = pl.transpose(self.R2)

            self.gs = ['I+I+I', 'R+R+I', 'C1+I', 'C3+I',
                       'I+R+R', 'R+I+R', 'Sm+R', 'Sp+R']

            self.gO = {"I+I+I": "",
                       "R+I+R": "sx*sz", "I+R+R": "sy*sz",
                       "Sp+R": "sz*sx*C4", "Sm+R": "sz*sy*C4",
                       "C1+I": "C4", "R+R+I": "C4*C4", "C3+I": "C4*C4*C4"}

            self.spaceGen = {"C4": np.matrix([[0, -1, 0],
                                              [1, 0, 0],
                                              [0, 0, 1]]),
                             "sx": np.diag([-1, 1, 1]),
                             "sy": np.diag([1, -1, 1]),
                             "sz": np.diag([1, 1, -1])}

            self.spinGen = {"C4":
                            np.matrix([[(1 - 1j)/np.sqrt(2), 0],
                                       [0, (1 + 1j)/np.sqrt(2)]]),
                            "sx": np.matrix([[0, 1j], [1j, 0]]),
                            "sy": np.matrix([[0, 1], [-1, 0]]),
                            "sz": np.matrix([[1j, 0], [0, -1j]])}

        elif self.sgroup == 23:
            self.R1 = pl.transpose(pl.matrix(((1/2., 1/2., -1/2.),
                                              (-1/2., 1/2., 1/2.),
                                              (1/2., -1/2., 1/2.))))
            self.R2 = ln.inv(self.R1)
            self.B = pl.transpose(self.R2)

            self.gs = ['I+I+I', 'R+R+I', 'I+R+R', 'R+I+R']

            self.gO = {"I+I+I": "", "R+I+R": "sx*sz",
                       "I+R+R": "sy*sz", "R+R+I": "sx*sy"}

            self.spaceGen = {"sx": np.diag([-1, 1, 1]),
                             "sy": np.diag([1, -1, 1]),
                             "sz": np.diag([1, 1, -1])}

            self.spinGen = {"sx": np.matrix([[0, 1j], [1j, 0]]),
                            "sy": np.matrix([[0, 1], [-1, 0]]),
                            "sz": np.matrix([[1j, 0], [0, -1j]])}

        elif self.sgroup == "cube":
            self.R1 = np.diag((1, 1, 1))
            self.R2 = self.R1
            self.B = self.R2

            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'Sp+I', 'Sm+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'Sp+R', 'Sm+R',
                       'C1+I', 'C2+I', 'C3+I', 'C1+R', 'C2+R', 'C3+R']

        else:
            raise ValueError("This SGroup value is not available.")

        self.orbs = []
        if not _orbs == '':
            if not len(_orbs):
                print("ERROR in SGroup.__init__: Orbs should be passed as an "
                      "array of strings. Left undefined. Use SGroup.SetOrbs.")
            else:
                self.SetOrbs(_orbs)

    def SetOrbs(self, orbs):
        list_orbs = ["x", "y", "z",
                     "xy", "yz", "3z^2-x^2-y^2", "zx", "x^2-y^2"]
        for o in orbs:
            if o not in list_orbs:
                raise ValueError("ERROR in SGroup.SetOrbs: "
                                 "The orbital %s does not exist "
                                 "in this module. Please change "
                                 "it, contact us or contribute "
                                 "to the project." % o)
            self.orbs.append(o)

    def GtoMatrix(self, g):
        if not isinstance(g, str):
            raise TypeError("g has to be a string describing the symmetry.")

        reg = re.compile(r'\w+')
        match = reg.findall(g)

        M = pl.zeros((3, 3))

        i = 0
        for m in match:
            # Identity
            if m == 'I':
                M[i, i] = 1
                i += 1
            # Reflexion along axis
            elif m == 'R':
                M[i, i] = -1
                i += 1
            # Reflexion along the positive diagonal formed by the next two
            elif m == 'Sp':
                M[i, i + 1] = 1
                M[i + 1, i] = 1
                i += 2
            # Reflexion along the negative diagonal formed by the next two
            elif m == 'Sm':
                M[i, i + 1] = -1
                M[i + 1, i] = -1
                i += 2
            # Rotation by 90 degrees
            elif m == 'C1':
                M[i, i + 1] = 1
                M[i + 1, i] = -1
                i += 2
            # Rotation by 180 degrees
            elif m == 'C2':
                M[i, i] = -1
                M[i + 1, i + 1] = -1
                i += 2
            # Rotation by 270 degrees
            elif m == 'C3':
                M[i, i + 1] = -1
                M[i + 1, i] = 1
                i += 2
            else:
                raise ValueError("I do not know this symbol for a symmetry.")
        return M

    def GtoOrb(self, g):
        """
        From a symmetry g, return the matrix representation in the orbital
        basis.

        The orbital representation is in real spherical harmonics. Functions
        of p- and d-orbitals are constructed with sympy. Looking at the way
        they transform into one another, the matrix representation is
        computed.
        g : str
            Text describing a symmetry. See initialization of self object.
        """
        if not isinstance(g, str):
            raise TypeError("g has to be a string describing the symmetry.")

        if g in self.GtoOrb_done:
            return self.GtoOrb_done[g]

        norb = len(self.orbs)

        M = self.GtoMatrix(g)
        MO = pl.zeros((norb, norb))

        f_orbs = {
            # p-orbitals
            "x": lambda d: 1.0*d[0],
            "y": lambda d: 1.0*d[1],
            "z": lambda d: 1.0*d[2],
            # d-orbitals
            "xy": lambda d: 1.0*d[0]*d[1],
            "yz": lambda d: 1.0*d[1]*d[2],
            "3z^2-x^2-y^2": lambda d: 1.0*(3*d[2]**2-d[0]**2-d[1]**2),
            "zx": lambda d: 1.0*(d[0]*d[2]),
            "x^2-y^2": lambda d: 1.0*(d[0]**2-d[1]**2)
        }

        x, y, z = sym.Symbol('x'), sym.Symbol('y'), sym.Symbol('z')

        d1 = np.transpose(np.matrix((x, y, z)))
        d2 = M.dot(d1)

        for i1, f1 in enumerate(self.orbs):
            for i2, f2 in enumerate(self.orbs):
                cond1 = (sym.simplify(f_orbs[f1](d2)) ==
                         sym.simplify(f_orbs[f2](d1)))
                cond2 = (sym.simplify(f_orbs[f1](d2)) ==
                         -sym.simplify(f_orbs[f2](d1)))
                if cond1:
                    MO[i1, i2] = 1
                elif cond2:
                    MO[i1, i2] = -1

        self.GtoOrb_done[g] = MO

        return MO
