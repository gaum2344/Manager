from .bases import PostProdBase
from .kptmesh import KptMesh, KptMesh1D
from .projectors import Projectors
from .hamiltonian import Hk
from .self_energy import SelfE
from .spectral_function import SpectralF
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
with_aac = True
try:
    from auxiliaryanalyticalcontinuation import AnalyticalContinuation
except ImportError:
    print("auxiliaryanalyticalcontinuation not installed. Cannot make"
          " the analytical continuation if necessary.")
    with_aac = False


class LoadOrderError(Exception):
    """Exception subclass. Just a custom error.
    """
    pass


class CorrelatedBandStructure(PostProdBase):
    """Object that gathers the necessary physical objects to construct
    the spectral function.

    This class exists to simplify post processing code as all the basic stuff
    is done in it.

    To build the spectral function, one needs to have:
        - A kptmesh (from an ABINIT .out file for example)
        - Projectors (from an ABINIT .ovlp file)
        - Self-energy (either in matsubara frequencies or in real frequencies).
          If in matsubara frequencies, the analytical continuation will be
          done on it using OmegaMaxEnt. The file must be a simple .dat file.
        - Hamiltonian (from an ABINIT .eig file).

    Once all data is written into such files, one can use this object like so:
    >>> bandstructure = CorrelatedBandStructure()
    >>> bandstructure.load_kptmesh("path/to/file.out",  # load kptmesh
                                   [dim1, dim2, dim3],
                                   sgroup)
    >>> bandstructure.load_projectors("path/to/file.ovlp",  # load projectors
                                      norb,
                                      nband)
    >>> bandstructure.load_self_energy("path/to/file.dat",  # load self energy
                                       units_factor=1)  # change units to have
                                                        # same units as H
    >>> bandstructure.load_hamiltonian("path/to/file.eig")  # load H
    # construct spectral function with fermi energy and broadening
    >>> bandstructure.construc_spectral_function(fermi_energy, broad=broad)
    # Then one can plot the spectral function using the
    >>> bandstructure.plot(mesh_indices_to_plot)
    """
    _loggername = "postprod.correlated_band_structure"

    def __init__(self, **kwargs):
        """CorrelatedBandStructure init method.

        Parameters
        ----------
        loglevel : int, optional
                   The logging level.
        """
        super().__init__(**kwargs)
        self.mesh = None
        self.projectors = None
        self.self_energy = None
        self.hamiltonian = None
        self.spectral_function = None

    def load_kptmesh(self, output_path, sgroup, kptmesh_dims=None, oneD=False,
                     **kwargs):
        """Load the KptMesh. Creates the KptMesh using the
        :class:`~manager.PostProd.kptmesh.KptMesh` class.

        Parameters
        ----------
        output_path : str
                      The path to the ABINIT .out file containing the kpt list.
        kptmesh_dims : array-like
                       The dimensions of the kpt mesh. Needed if oneD is False.
        sgroup : int
                 The sgroup of the system.
        oneD : bool, optional
               If True, only a 1D kptmesh will be used. This simplifies
               symetries considerations (no symetries applied) for a
               simple kpt path in BZ. For example when doing EPH calculations
               with the Frozen Phonon methods, symetries could be broken
               and it could be hard to input all symetries.
        """
        self._logger.info(f"Loading kptmesh from {output_path}.")
        if not oneD:
            if kptmesh_dims is None:
                raise ValueError("Need dimensions for the kptmesh.")
            self.mesh = KptMesh.read(output_path, kptmesh_dims, sgroup,
                                     **kwargs)
        else:
            self.mesh = KptMesh1D.read(output_path, sgroup, **kwargs)

    def load_projectors(self, projectors_path, norb, nband):
        """Load the projectors. Creates the KptMesh using the
        :class:`~manager.PostProd.projectors.Projectors` class.

        Parameters
        ----------
        projectors_path : str
                          The path to the ABINIT .ovlp file containing the
                          projectors.
        norb : int
               The number of orbitals.
        nband : int
                The number of bands.
        """
        self._logger.info(f"Loading projectors from {projectors_path}.")
        if self.mesh is None:
            raise LoadOrderError("Load KptMesh before loading projectors.")
        self.projectors = Projectors(norb, nband, self.mesh.nkpt)
        self.projectors.read(projectors_path)

    def _preload_self(self):
        if self.projectors is None:
            raise LoadOrderError("Load projectors before loading self energy!")

    def load_self_energy(self, self_energy_path, units_factor=1, **kwargs):
        """Load the self energy in real frequencies.
        Creates the self energy array using the
        :class:`~manager.PostProd.self_energy.SelfE` class.

        Parameters
        ----------
        self_energy_path : str
                           The path to the self_energy data file.
        units_factor : float, optional
                       If not None, all the self energy data will be multiplied
                       by this number after being loaded.
        """
        self._logger.info(f"Loading self-energy in real frequencies"
                          f" from {self_energy_path}.")
        self._preload_self()
        self.self_energy = SelfE.read(self_energy_path,
                                      units_factor=units_factor, **kwargs)
        self.self_energy.load_projectors(self.projectors)

    def load_self_energy_matsubara(self, self_path, dc,
                                   intrapolation_mesh=None,
                                   intrapolation_degree=1,
                                   units_factor=1,
                                   output_path=None, **kwargs):
        """Load the self energy in matsubara frequencies.
        It first computes the analytical continuation using OmegaMaxEnt to get
        the self-energy in real frequencies.
        Creates the self energy array using the
        :class:`~manager.PostProd.self_energy.SelfE` class.

        Parameters
        ----------
        self_path : str
                    The path to the self_energy data file in
                    matsubara frequencies.
        dc : float
             The doulb-counting energy.
        intrapolation_mesh : array-like, optional
                             Gives the intrapolation mesh data. Must be of the
                             form [min_freq, max_freq, delta_freq].
        units_factor : float, optional
                       If not None, all the self energy data will be multiplied
                       by this number after being loaded.
        output_path : str, optional
                      The relative path compared to the self_energy path in
                      matsubara frequencies where the self_energy in real
                      frequencies will be saved.
        kwargs : other kwargs are passed directly to the
                 :class:`auxiliaryanalyticalcontinuation:AnalyticalContinuation`
                 class and to the 'load_self_energy' method.
        """
        if not with_aac:
            self._logger.critical("Dependency error:"
                                  " auxiliaryanalyticalcontinuation")
            raise ModuleNotFoundError("The module auxiliaryanalytical"
                                      "continuation must be installed to"
                                      " make the analytical continuation.")
        self._logger.info(f"Loading self-energy in matsubara frequencies"
                          f" from {self_path}.")
        self._preload_self()
        # self energy in matsubara frequencies =>
        # make the analytical continuation
        freq_min = kwargs.pop("freq_min", None)
        freq_max = kwargs.pop("freq_max", None)
        if dc is None:
            raise ValueError("Give DC energy for analytical continuation.")
        if output_path is None:
            output_path = "self_w.dat"
        output_path = os.path.join(os.path.dirname(self_path), output_path)
        self._logger.info("Doing analytical continuation for self_energy.")
        AnalyticalContinuation.from_file(self_path, dc,
                                         mesh=intrapolation_mesh,
                                         degree=intrapolation_degree,
                                         output_path=output_path,
                                         **kwargs)
        self._logger.info(f"Analytical continuation done: self energy in real"
                          f" frequencies saved at: {output_path}")
        self.load_self_energy(output_path,
                              freq_min=freq_min, freq_max=freq_max,
                              units_factor=units_factor)

    def load_hamiltonian(self, eig_path):
        """Load the hamiltonian.
        Creates the hamiltonian using the
        :class:`~manager.PostProd.hamiltonian.Hk` class.

        Parameters
        ----------
        eig_path : str
                   The path to the ABINIT .eig file.
        """
        if self.projectors is None:
            raise LoadOrderError("Load projectors before loading hamiltonian.")
        self._logger.info(f"Loading the Hamiltonian from {eig_path}.")
        self.hamiltonian = Hk(self.mesh.nkpt, self.projectors.nband)
        self.hamiltonian.read(eig_path)

    def construct_spectral_function(self, fermi_energy, broad=0.01,
                                    path=None, overwrite=False,
                                    nfreq=None, freqmin=None, freqmax=None):
        """Constructs the spectral function.

        Uses the :class:`~manager.PostProd.spectral_function.SpectralF` class
        to do so.

        Parameters
        ----------
        fermi_energy : float
                       The fermi energy of the system. The units must be the
                       same as the self-energy and the hamiltonian loaded
                       previously.
        broad : float, optional
                The spectral function artificial broadening.
        path : str, optional
               If not None, the spectral function will be read from this
               file if it exists. If not, it will be constructed and then
               saved to this location.
        overwrite : bool, optional
                    If True, if a path is given and the file already exists,
                    the spectral function will be reconstructed and resaved.
        """
        if self.hamiltonian is None:
            raise LoadOrderError("Load Hamiltonian before constructing"
                                 " spectral function.")
        self_e = self.self_energy
        if self_e is not None:
            self._logger.info("Projecting self energy in the band basis.")
            self_e = self_e.self_energy_bandbasis
            self._freqs = self_e.freqs
        else:
            if nfreq is None:
                raise ValueError("Give nfreq if no self energy.")
            if freqmin is None:
                raise ValueError("Give freqmin if no self energy.")
            if freqmax is None:
                raise ValueError("Give freqmax if no self energy.")
            freqs = np.linspace(freqmin, freqmax, nfreq)
            self._freqs = freqs
        self.spectral_function = SpectralF(self.mesh.nkpt,
                                           self.projectors.nband,
                                           len(self._freqs))
        if path is not None and not overwrite:
            if os.path.exists(path):
                self._logger.info(f"Loading spectral function from {path}")
                self.spectral_function.read(path)
                return
        # if we are here, file does not exists. Compute spectral function
        self._logger.info("Computing spectral function.")
        self.spectral_function.construct(self.hamiltonian,
                                         self._freqs,
                                         fermi_energy,
                                         self_e,
                                         broad=broad)
        self._logger.debug("Spectral function computed.")
        if path is not None:
            self._logger.info(f"Writing spectral function to {path}.")
            if overwrite and os.path.exists(path):
                os.remove(path)
            self.spectral_function.write(path)

    def plot(self, mesh_indices, show=True):
        """Shows the correlated band structure in the form of an image.

        This method returns the matplotlib objects of the plot. Useful
        for plotting something on top.

        Parameters
        ----------
        mesh_indices : list-like
                       The indices of the KptMesh to show.
        show : bool, optional
               If True, the plot is shown.

        Returns
        -------
        figure : The matplotlib figure.
        axis : The matplotlib axis.
        """
        if self.spectral_function is None:
            raise LoadOrderError("Compute spectral function"
                                 " before showing it!!")
        self._logger.info("Plotting spectral function.")

        if isinstance(self.mesh, KptMesh1D):
            bands = self.spectral_function[:, mesh_indices, :]
        else:
            kpts_indices = [self.mesh[x[0], x[1], x[2]] for x in mesh_indices]
            bands = self.spectral_function[:, kpts_indices, :]
        nkpt = self.mesh.nkpt
        freqs = self._freqs
        bands = np.sum(bands, axis=-1)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.imshow(bands, origin='lower', aspect='auto',
                        extent=(0, nkpt, min(freqs), max(freqs)),
                        cmap=matplotlib.cm.Greys)
        fig.colorbar(cax)
        ax.set_xlabel("kpt")
        ax.set_ylabel("E (eV)")
        if show:
            self._logger.info("Showing image.")
            plt.show()
        return fig, ax

    def _get_data(self):
        # correlated bandstructure data is spectral function
        if self.spectral_function is None:
            raise LoadOrderError("Construct Spectral function"
                                 " before using it (Duh)!")
        return self.spectral_function._data
